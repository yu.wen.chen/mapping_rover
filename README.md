# Mapping Rover 
![](imgs/rover.jpg)

## Launch sensors for SLAM

```bash
# publish description, launch rslidar, xsens_imu, ublox_gps, ntrip, realsense_d435
roslaunch mapping_rover mapping_rover_slam.launch 
# launch hdl_graph_slam (in other package)
roslaunch hdl_graph_slam hdl_graph_slam_urbant.launch

```
## Record topics 

* Camera image: /camera/color/image_raw/compressed
* Camera info:  /camera/color/camera_info 
* IMU data:     /imu/data 
* Pointcloud:   /front/rslidar_points 
* TF:           /tf , /tf_static 
* GPS:          /ublox_gps/fix

```bash
roslaunch mapping_rover rosbag_record.launch
```

## Save map and Dump

```bash
rosservice call /hdl_graph_slam/dump "destination: '<full_path>/<your_map_name>'"

rosservice call /hdl_graph_slam/save_map "utm: false
resolution: 0.05
destination: '<full_path>/<your_map_name>.pcd'"
```
